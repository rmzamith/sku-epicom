package sku.service;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import sku.controller.dto.ImportSkuDTO;
import sku.epicom.api.resource.SkuMarketplaceResource;
import sku.epicom.api.specification.PassiveOpeartionTypes;
import sku.model.Sku;

@Component
public class SkuFactory {
	
	@Autowired
	private SkuMarketplaceResource skuMarketplaceChannel;	
	
	public Sku createNewSkuEntryFromImportedDto( ImportSkuDTO skuData ) {
    	String tipo = skuData.getTipo();
    	PassiveOpeartionTypes operation = PassiveOpeartionTypes.valueOf(tipo);
    	if( operation!= PassiveOpeartionTypes.criacao_sku ) {
    		String errorMsg = String.format(
    				"Was expecting operation type %s, but was %s",
    				PassiveOpeartionTypes.criacao_sku, tipo );
    		throw new IllegalAccessError(errorMsg);
    	}
    	
    	Map<String, String> parametros = skuData.getParametros();
    	String idProduto = parametros.get("idProduto");
    	String idSku = parametros.get("idSku");
    	Sku skuMarketplaceData = skuMarketplaceChannel.getResourceData(idProduto, idSku);
    	
		return skuMarketplaceData;
    }
}
