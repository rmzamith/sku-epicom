package sku.epicom.api.resource;

import org.springframework.stereotype.Component;

import sku.model.Sku;

@Component
public class SkuMarketplaceResource extends AbstractResource<Sku>{
	
	@Override
	protected String getResourceUrl() {
		return String.format("/marketplace/produtos/%s/skus/%s", idProduto, idSku);
	}

	@Override
	protected Class<Sku> getResponseClass() {
		return Sku.class;
	}
	
	public Sku getResourceData(String idProduto, String idSku)
	{
		this.idProduto = idProduto;
		this.idSku = idSku;
		return super.getResourceData();
	}
	
	@Deprecated
	@Override
	public Sku getResourceData()
	{
		throw new UnsupportedOperationException(
				"This method shouldn't be called this way. "+
				"See SkuMarketplaceChannel.getChannelData(String idProduto, String idSku)");
	}
	
	private String idProduto;
	
	private String idSku;
}
