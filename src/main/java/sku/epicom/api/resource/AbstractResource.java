package sku.epicom.api.resource;

import java.nio.charset.Charset;

import org.apache.tomcat.util.codec.binary.Base64;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

abstract class AbstractResource<T> {
	
	@Value("${sku.epicom.endpoint.key}")
	private String KEY;
	@Value("${sku.epicom.endpoint.token}")
	private String TOKEN;
	@Value("${sku.epicom.endpoint.url}")
	private String ENDPOINT_URL;
	
	protected abstract String getResourceUrl();
	protected abstract Class<T> getResponseClass();
	
	public T getResourceData()
	{
		String resourceUrl = getResourceUrl();
		String url = String.format("%s%s", ENDPOINT_URL, resourceUrl );
		
		RestTemplate restTemplate = new RestTemplate();
		ResponseEntity<T> response = 
				restTemplate.exchange(
						url, 
						HttpMethod.GET, 
						new HttpEntity<T>(createAuthHeaders()), 
						getResponseClass());
		T body = response.getBody();
		
		return body;
		
	}
	
	private HttpHeaders createAuthHeaders(){
	   return new HttpHeaders(){
		private static final long serialVersionUID = 7943930655941559672L;
		{
	         String auth = KEY + ":" + TOKEN;
	         byte[] encodedAuth = Base64.encodeBase64( 
	            auth.getBytes(Charset.forName("US-ASCII")) );
	         String authHeader = "Basic " + new String( encodedAuth );
	         set( "Authorization", authHeader );
	      }
	   };
	}
}
