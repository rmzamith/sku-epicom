package sku.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import sku.model.Sku;

@RepositoryRestResource(collectionResourceRel = "sku", path = "sku")
public interface SkuRepository extends MongoRepository<Sku, String> {

	List<Sku> findByNome(@Param("nome") String nome);
	
	@Query(value="{$query:{ 'preco' : {'$gt' : 10.0, '$lt' : 40.0}, 'disponivel': true}, $orderby:{'preco':1}}")
	List<Sku> findAvaliableSkusByPriceBetween10And40();
	
	@Query(value="{$query:{ 'preco' : {'$gt' : ?0, '$lt' : ?1}, 'disponivel': true}, $orderby:{'preco':1}}")
	List<Sku> findAvaliableSkusByPriceInterval(@Param("from")Double from, @Param("to")Double to);

}
