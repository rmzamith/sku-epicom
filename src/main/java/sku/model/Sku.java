package sku.model;

import org.springframework.data.annotation.Id;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Sku {

	@Id 
	private String id;
	private String nome;
	private String nomeReduzido;
	private String codigo;
	private String modelo;
	private String ean;
	private String url;
	private Boolean foraDeLinha;
	private Double preco;
	private Double precoDe;
	private Boolean disponivel;
	private Integer estoque;
	private Boolean ativo;
	private String codigoCategoria;
	
	/**
	 * @return the nome
	 */
	public String getNome() {
		return nome;
	}
	/**
	 * @param nome the nome to set
	 */
	public void setNome(String nome) {
		this.nome = nome;
	}
	/**
	 * @return the nomeReduzido
	 */
	public String getNomeReduzido() {
		return nomeReduzido;
	}
	/**
	 * @param nomeReduzido the nomeReduzido to set
	 */
	public void setNomeReduzido(String nomeReduzido) {
		this.nomeReduzido = nomeReduzido;
	}
	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}
	/**
	 * @return the codigo
	 */
	public String getCodigo() {
		return codigo;
	}
	/**
	 * @param codigo the codigo to set
	 */
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	/**
	 * @return the modelo
	 */
	public String getModelo() {
		return modelo;
	}
	/**
	 * @param modelo the modelo to set
	 */
	public void setModelo(String modelo) {
		this.modelo = modelo;
	}
	/**
	 * @return the ean
	 */
	public String getEan() {
		return ean;
	}
	/**
	 * @param ean the ean to set
	 */
	public void setEan(String ean) {
		this.ean = ean;
	}
	/**
	 * @return the url
	 */
	public String getUrl() {
		return url;
	}
	/**
	 * @param url the url to set
	 */
	public void setUrl(String url) {
		this.url = url;
	}
	/**
	 * @return the foraDeLinha
	 */
	public Boolean getForaDeLinha() {
		return foraDeLinha;
	}
	/**
	 * @param foraDeLinha the foraDeLinha to set
	 */
	public void setForaDeLinha(Boolean foraDeLinha) {
		this.foraDeLinha = foraDeLinha;
	}
	/**
	 * @return the preco
	 */
	public Double getPreco() {
		return preco;
	}
	/**
	 * @param preco the preco to set
	 */
	public void setPreco(Double preco) {
		this.preco = preco;
	}
	/**
	 * @return the precoDe
	 */
	public Double getPrecoDe() {
		return precoDe;
	}
	/**
	 * @param precoDe the precoDe to set
	 */
	public void setPrecoDe(Double precoDe) {
		this.precoDe = precoDe;
	}
	/**
	 * @return the disponivel
	 */
	public Boolean getDisponivel() {
		return disponivel;
	}
	/**
	 * @param disponivel the disponivel to set
	 */
	public void setDisponivel(Boolean disponivel) {
		this.disponivel = disponivel;
	}
	/**
	 * @return the estoque
	 */
	public Integer getEstoque() {
		return estoque;
	}
	/**
	 * @param estoque the estoque to set
	 */
	public void setEstoque(Integer estoque) {
		this.estoque = estoque;
	}
	/**
	 * @return the ativo
	 */
	public Boolean getAtivo() {
		return ativo;
	}
	/**
	 * @param ativo the ativo to set
	 */
	public void setAtivo(Boolean ativo) {
		this.ativo = ativo;
	}
	/**
	 * @return the codigoCategoria
	 */
	public String getCodigoCategoria() {
		return codigoCategoria;
	}
	/**
	 * @param codigoCategoria the codigoCategoria to set
	 */
	public void setCodigoCategoria(String codigoCategoria) {
		this.codigoCategoria = codigoCategoria;
	}
}