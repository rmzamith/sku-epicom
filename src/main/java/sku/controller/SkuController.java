package sku.controller;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.BasePathAwareController;
import org.springframework.data.rest.webmvc.RepositoryLinksResource;
import org.springframework.hateoas.EntityLinks;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.LinkBuilder;
import org.springframework.hateoas.Resource;
import org.springframework.hateoas.ResourceAssembler;
import org.springframework.hateoas.ResourceProcessor;
import org.springframework.hateoas.Resources;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import sku.controller.dto.ImportSkuDTO;
import sku.model.Sku;
import sku.repository.SkuRepository;
import sku.service.SkuFactory;

@BasePathAwareController
public class SkuController 
	implements ResourceProcessor<RepositoryLinksResource>, ResourceAssembler<Sku, Resource<Sku>> {
	
	private static final Log LOG = LogFactory.getLog( SkuController.class );
	
	private final SkuRepository skuRepository;
	
	private final SkuFactory skuFactory;
	
	@Autowired
    private EntityLinks entityLinks;
	
	@Autowired
    public SkuController(SkuRepository skuRepository, SkuFactory skuFactory) { 
		this.skuRepository = skuRepository;
		this.skuFactory = skuFactory;
    }
    
    @RequestMapping(method = RequestMethod.POST, value = "/sku/import") 
    public @ResponseBody ResponseEntity<?> importSku( @RequestBody final ImportSkuDTO skuData ) {
    	try {
    		Sku importedSku = skuFactory.createNewSkuEntryFromImportedDto( skuData );
    		Sku savedSku = skuRepository.save(importedSku);
    		return ResponseEntity.created(new URI(buildSkuLink(savedSku))).body(toResource(savedSku)); 
    	} catch (Exception e) {
        	if(LOG.isErrorEnabled()){
        		LOG.error("Bad Request.", e);
        	}
        	return ResponseEntity.badRequest().build(); 
        }
    }
    
    @RequestMapping(method = RequestMethod.POST, value = "/sku/importList") 
    public @ResponseBody ResponseEntity<?> importListSku( @RequestBody final List<ImportSkuDTO> skuListData ) {
    	try {
    		List<Sku> importedSkus = new ArrayList<Sku>();
    		skuListData.forEach( skuData ->{ 
    			Sku importedSku = skuFactory.createNewSkuEntryFromImportedDto( skuData );
    			importedSkus.add(importedSku);
    		});
    		
    		List<Sku> savedSkus = skuRepository.save( importedSkus );
    		
    		List<Resource<Sku>> skuResources = new ArrayList<Resource<Sku>>();
    		savedSkus.forEach( savedSku -> { 
    			skuResources.add(toResource(savedSku)); 
    		});
    		
    		LinkBuilder lb = entityLinks.linkFor(Sku.class);
    		Resources<Resource<Sku>> resources = new Resources<Resource<Sku>>(skuResources);
    		return ResponseEntity.created(new URI(lb.toString())).body(resources); 
    		
    	} catch (Exception e) {
        	if(LOG.isErrorEnabled()){
        		LOG.error("Bad Request.", e);
        	}
        	return ResponseEntity.badRequest().build(); 
        }
    }
    
    @Override
    public RepositoryLinksResource process(RepositoryLinksResource resource) {

        LinkBuilder lb = entityLinks.linkFor(Sku.class);
        resource.add(new Link(lb.toString() + "/import", "importSku"));
        resource.add(new Link(lb.toString() + "/importList", "importListSku"));
        return resource;
    }

    @Override
    public Resource<Sku> toResource(Sku sku) {
        String linkUrl = buildSkuLink(sku);
        Resource<Sku> resource = new Resource<Sku>(sku);
        resource.add(new Link(linkUrl).withSelfRel()); 
        return resource;
    }
    
    private String buildSkuLink(Sku sku)
    {
    	LinkBuilder lb = entityLinks.linkFor(Sku.class);
        String linkUrl = String.format("%s/%s", lb.toString(), sku.getId() );
        return linkUrl;
    }
    
}
