package sku.controller.dto;

import java.util.Date;
import java.util.Map;

public class ImportSkuDTO 
{
	private String tipo;
	private Date dataEnvio;
	private Map<String, String> parametros;
	
	public String getTipo() {
		return tipo;
	}
	
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	
	public Date getDataEnvio() {
		return dataEnvio;
	}
	
	public void setDataEnvio(Date dataEnvio) {
		this.dataEnvio = dataEnvio;
	}
	
	public Map<String, String> getParametros() {
		return parametros;
	}
	
	public void setParametros(Map<String, String> parametros) {
		this.parametros = parametros;
	}

}
