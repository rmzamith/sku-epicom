package sku.test;

import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.patch;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.boot.test.WebIntegrationTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.context.WebApplicationContext;

import com.mongodb.MongoClient;

import de.flapdoodle.embed.mongo.MongodExecutable;
import de.flapdoodle.embed.mongo.MongodProcess;
import de.flapdoodle.embed.mongo.MongodStarter;
import de.flapdoodle.embed.mongo.config.MongodConfigBuilder;
import de.flapdoodle.embed.mongo.config.Net;
import de.flapdoodle.embed.mongo.distribution.Version;
import de.flapdoodle.embed.process.runtime.Network;
import sku.Application;
import sku.model.Sku;
import sku.repository.SkuRepository;


@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(Application.class)
@WebIntegrationTest
public class SkuCrudIntegrationTest {
	
	private List<Sku> SKU_LIST = new ArrayList<Sku>();
    
    private static final MongodStarter starter = MongodStarter.getDefaultInstance();
    private MongodExecutable _mongodExe;
    private MongodProcess _mongod;
	private MongoClient _mongo;

	@Autowired
    private SkuRepository skuRepository;
    @Autowired
    private WebApplicationContext webApplicationContext;
    private MockMvc mockMvc;
    
    private void setupMockDatabase() throws IOException
    {
    	_mongodExe = starter.prepare(new MongodConfigBuilder()
                .version(Version.Main.PRODUCTION)
                .net(new Net(12345, Network.localhostIsIPv6()))
                .build());
        _mongod = _mongodExe.start();

        _mongo = new MongoClient("localhost", 12345);
    }
    
    private void setupSkuApplication()
    {
    	this.mockMvc = webAppContextSetup(webApplicationContext).build();

        this.skuRepository.deleteAll();

        Sku sku = new Sku();
        sku.setId("12345");
        sku.setNome("Produto 1");
        
        this.SKU_LIST.add(skuRepository.save(sku));
    }

    @Before
    public void setup() throws Exception {
    	setupMockDatabase();
    	setupSkuApplication();
    }
    
    @Test
    public void testAccessOfNonexistentEntity() throws Exception {

    	 mockMvc.perform(get("/sku/qwe123"))
                 .andExpect(status().isNotFound());
    }

    @Test
    public void testAccessOfValidEntity() throws Exception {

    	MediaType contentType = new MediaType("application",
    			"hal+json");
    	
    	 mockMvc.perform(get("/sku/"+this.SKU_LIST.get(0).getId()))
                 .andExpect(status().isOk())
                 .andExpect(content().contentType(contentType))
                 .andExpect(jsonPath("$._links.self.href", is("http://localhost/sku/" + this.SKU_LIST.get(0).getId())))
                 .andExpect(jsonPath("$.nome", is("Produto 1")));
    }
    
    @Test
    public void testUpdateOfValidEntity() throws Exception {

    	String nameUpdated = "Produto 2";
    	String updateJson = String.format("{\"nome\":\"%s\"}", nameUpdated);
    	mockMvc.perform(patch("/sku/"+this.SKU_LIST.get(0).getId())
	 		.contentType(MediaType.APPLICATION_JSON)
	 		.content(updateJson.getBytes()))
	 		.andExpect(status().isNoContent());
    	
    	mockMvc.perform(get("/sku/"+this.SKU_LIST.get(0).getId()))
    		.andExpect(status().isOk())
	 		.andExpect(jsonPath("$._links.self.href", is("http://localhost/sku/" + this.SKU_LIST.get(0).getId())))
            .andExpect(jsonPath("$.nome", is(nameUpdated)));
    }
    
    @Test
    public void testDeleteOfValidEntity() throws Exception {

    	 mockMvc.perform(delete("/sku/"+this.SKU_LIST.get(0).getId()))
    	 	.andExpect(status().isNoContent());
    	 
    	 mockMvc.perform(get("/sku/"))
 	 		.andExpect(status().isOk())
 	 		.andExpect(jsonPath("$.page.totalElements", is(0))); 
    }
    
    @Test
    public void testCreationOfNewEntity() throws Exception {
    	mockMvc.perform(post("/sku/")
	 		.contentType(MediaType.APPLICATION_JSON)
	 		.content("{\"id\":\"654987\", \"nome\":\"Produto 3\"}".getBytes()))
	 		.andExpect(status().isCreated());
    }
    
    private byte[] getSkuImportMockDataJsonBytes() throws IOException
	{
    	FileInputStream fileInputStream = null;
    	try
    	{
    		ClassLoader classLoader = getClass().getClassLoader();
	        File file = new File(classLoader.getResource("criacao_sku.json").getFile());
	        byte[] bFile = new byte[(int) file.length()];
	        
	        fileInputStream = new FileInputStream(file);
		    fileInputStream.read(bFile);
		    return bFile;
    	}
    	finally
    	{
    		if( fileInputStream != null)
    		{
    			fileInputStream.close();
    		}
    	}
	}
    
    @Test
    public void testImportOfValidSkuNotificationJsonList() throws Exception {
    	
    	byte[] mockData = getSkuImportMockDataJsonBytes();

    	mockMvc.perform(post("/sku/importList")
	 		.contentType(MediaType.APPLICATION_JSON)
	 		.content(mockData))
	 		.andExpect(status().isCreated());
    	 
    	 mockMvc.perform(get("/sku/"))
 	 		.andExpect(status().isOk())
 	 		.andExpect(jsonPath("$.page.totalElements", is(101))); 
    }
    
    
    @After
    public void tearDown() throws Exception {
    	_mongo.close();
        _mongod.stop();
        _mongodExe.stop();
    }
}

