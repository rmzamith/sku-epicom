package sku.test;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import sku.epicom.api.resource.SkuMarketplaceResource;
import sku.model.Sku;
import sku.test.config.AppUnitTestConfig;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration( classes = { SkuMarketplaceResource.class,
								   AppUnitTestConfig.class})
public class EpicomSkuMarketplaceTest {

	@Autowired
	SkuMarketplaceResource skuMarketplaceResource;
	
	@Test
	public void testRetrieveValidDataFromEpicomMarketplaceAPI() throws Exception {
		
		String idSku = "322358";
		String idProduto = "270229";
		Sku channelData = skuMarketplaceResource.getResourceData( idProduto, idSku );
		Assert.assertEquals(channelData.getId(), idSku);		
	}
}
