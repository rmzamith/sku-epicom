package sku.test;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({
	SkuCrudIntegrationTest.class,
	EpicomSkuMarketplaceTest.class
})
public class TestSuite {
}
