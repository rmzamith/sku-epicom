# README #

Projeto [desafio](https://markdownshare.com/view/ca37195e-4295-487e-b26d-e003b1b95f0f) para admissão na Epicom.

Artefato compilado para download [sku-0.0.1-SNAPSHOT.jar](https://bitbucket.org/rmzamith/sku-epicom/downloads/sku-0.0.1-SNAPSHOT.jar)

## Sobre o Projeto ##

Este projeto é um protótipo de uma API REST HATEAOAS para o CRUD de informações sobre SKUs. Foi desenvolvido em Java, com o framework [Spring](https://spring.io/), e se conecta com uma base de dados não relacional [MongoDB](https://www.mongodb.com/) para persistência dos dados.

A inicialização da applicação é feita pelo [Spring Boot](http://projects.spring.io/spring-boot/). Em sua inicialização, é levantada uma instância do [Tomcat](http://tomcat.apache.org/), configurada para utilizar o protocolo HTTPS. Caso queira alterar as configurações do Tomcat, verificar [aqui](http://docs.spring.io/spring-boot/docs/current/reference/html/howto-embedded-servlet-containers.html). Mais especificamente, para alterar as configurações do SSL verificar [aqui](http://docs.spring.io/spring-boot/docs/current/reference/html/howto-embedded-servlet-containers.html#howto-configure-ssl).

Outro aspecto da aplicação que pode ser configurado é a conexão com o banco de dados. O Spring Boot utiliza uma configuração padrão para conexão com o MongoDB. Caso queira alterar essas configurações, verificar [aqui](http://docs.spring.io/spring-boot/docs/current/reference/html/boot-features-nosql.html#boot-features-mongodb).

As configurações de conexão com a API da Epicom estão definidas em `/src/main/resources/application.properties`

A API oferece as seguintes funcionalidades:

* Efetuar operações de CRUD e de busca para uma entidade de SKU.
* Receber notificações da API da Epicom para criação de entidades SKU.
* Filtrar e ordenar de forma ascendente os SKUs válidos com preço entre 10.0 e 40.0.

## Dependências do Projeto ##

* Para execução é necessário ter instalado Java (1.8) e uma instancia de servidor MongoDB (3.2 +).
* Para compilação, além dos requisitos para execução também será necessário Maven (3.3.9)
* Instruções para instalar o [Java](https://docs.oracle.com/javase/8/docs/technotes/guides/install/install_overview.html)
* Instruções para instalar o [MongoDB](https://docs.mongodb.com/manual/installation/)
* Instruções para instalar o [Maven](https://maven.apache.org/install.html)

## Como Compilar ##
1.Em um shell, navagar até o diretório raiz do projeto.

2.Executar o seguinte comando para executar os testes e compilar:

```
#!shell

$ mvn package
```
3.Caso queira compilar sem rodar os testes executar o comando com o seguinte parâmetro:
```
#!shell

$ mvn package -Dmaven.test.skip=true
```
4.Será gerado um diretório `/target` dentro do diretório raiz do projeto, contendo os artefatos compilados.

OBS: Caso não queira compilar, um jar compilado se encontra disponível [aqui](https://bitbucket.org/rmzamith/sku-epicom/downloads/sku-0.0.1-SNAPSHOT.jar)

## Como Executar ##
1.Iniciar uma instâcia do servidor de MongoDB.

2.Após compilada a API, acessar a partir de um shell o diretório `/target` dentro do diretório raiz do projeto.

3.Executar o seguinte comando:
```
#!shell

$ java -jar sku-0.0.1-SNAPSHOT.jar
```
4.Se estivar utilizando as configurações padrões, acessar a aplicação pela url `https://localhost:8443/`

## Como Utilizar a API ##

A API foi desenvolvida usando o conceito de [HATEOAS](https://en.wikipedia.org/wiki/HATEOAS). Essa arquitetura define a maneira na qual o aplicativo cliente interage com o servidor, fornecendo links hipermídia no interior dos modelos de recursos disponíveis. As operações de CRUD são feitas através de verbos HTTP GET, PATCH, POST e DELETE.

As funcionalidades da API estão listadas no caminho raiz da aplicação (Se estiver utilizando as configurações padrões, acessar através da url `https://localhost:8443/`). São elas:

* CRUD e buscas de objetos SKU, acessada por `/sku`
* Importação de um objeto SKU, acessada por `/sku/import`
* Importação de uma lista de objetos SKU, acessada por `/sku/importList`

É necessário autenticação para acessar os recursos do sistema. Para autenticar, utilize o usuário 'epicom' e a senha '123Mudar'. É possivel enviar a autenticação nos cabeçalhos de requisição HTTPS, adicionando o cabeçalho Authorization com um valor no formato `Basic <chave>`, onde `<chave>` deve ser `<usuário>:<senha>` codificado em base64. Caso esteja utilizando as configurações padrões, `<chave>` deve ser substituida pelo valor `ZXBpY29tOjEyM011ZGFy` ('epicom:123Mudar' em base64).

### Operações de CRUD ###

#### Modelo ####
* O modelo da entidade que representa o SKU é o mesmo do [SkuMarketplace](https://sandboxmhubapi.epicom.com.br/v1/ResourceModel?modelName=SkuMarketplace) na API da Epicom, com a diferença que o recurso apresenta também uma propriedade `_links` com a referência para o proprio recurso SKU.

#### GET /sku ####
* Retorna uma lista paginada de todos os registros de SKUs na base.

##### Requisição #####

###### Parâmetros na URL ######
* Nenhum.

###### Body ######
* Nenhum.

##### Resposta #####
* HTTP 200 em caso de sucesso.
* Retorna um recurso Json HATEOAS contendo uma lista de SKUs cadastrados na base.
A Lista é paginada. Informações sobre como navegar pelas páginas se encontram na propriedade `_links` do Json.
O conteúdo da página se encontra na propriedade `_embedded`.
Informações sobre a página se encontram na propriedade `page`.


#### POST /sku ####
* Registra na aplicação uma nova entidade representando um SKUs.

##### Requisição #####

###### Parâmetros na URL ######
* Nenhum.

###### Body ######
* Um Json seguindo o formato do modelo de um SKU.

##### Resposta #####
* HTTP 201 em caso de sucesso.
* Retorna um recurso Json HATEOAS contendo o SKU cadastrado na base.


#### GET /sku/{idSku} ####
* Retorna um recurso de SKU pelo idSku forneciso.

##### Requisição #####

###### Parâmetros na URL ######
* idSku - Id do SKU que se deseja buscar.

###### Body ######
* Nenhum.

##### Resposta #####
* HTTP 200 em caso de sucesso.
* Um Json HATEOAS no formato do modelo de um SKU.

#### PATCH /sku/{idSku} ####
* Atualiza um recurso de SKU pelo idSku fornecido.

##### Requisição #####

###### Parâmetros na URL ######
* idSku - Id do SKU que se deseja atualizar.

###### Body ######
* Um Json seguindo o formato do modelo de um SKU, contendo as propriedades e os respectivos valores a serem atualizados.

##### Resposta #####
* HTTP 204 em caso de sucesso.
* Um Json HATEOAS no formato do modelo de um SKU com o SKU atualizado.

#### DELETE /sku/{idSku} ####
* Remove o registro de um recurso de SKU pelo idSku fornecido.

##### Requisição #####

###### Parâmetros na URL ######
* idSku - Id do SKU que se deseja atualizar.

###### Body ######
* Nenhum.

##### Resposta #####
* HTTP 204 em caso de sucesso.


### Operações de Busca ###

Acessando `/sku/search` são listadas as buscas customizadas disponíveis. Algumas delas são parametrizadas.

#### GET /sku/search/findByNome{?nome} ####
* Busca um SKU pelo nome fornecido.

##### Requisição #####

###### Parâmetros na URL ######
* nome - Nome do SKU que se deseja buscar..

###### Body ######
* Nenhum.

##### Resposta #####
* HTTP 200 em caso de sucesso.
* Uma lista com os SKUs que possuem o nome fornecido.

#### GET /sku/search/findAvaliableSkusByPriceInterval{?from,to} ####
* Busca e ordena de forma ascendente os SKUs disponíveis com o preço no intervalo definido.

##### Requisição #####

###### Parâmetros na URL ######
* from - Valor do início do intervalo de preço.
* to - Valor do fim do intervalo de preço. 

###### Body ######
* Nenhum.

##### Resposta #####
* HTTP 200 em caso de sucesso.
* Uma lista com os SKUs disponíveis que possuem preço dentro do intervalo de preço definido.

#### GET /sku/search/findAvaliableSkusByPriceBetween10And40 ####
* Busca e ordena de forma ascendente os SKUs disponíveis que estão no intervalo de preço entre 10.0 e 40.0. (Item do desafio)

##### Requisição #####

###### Parâmetros na URL ######
* Nenhum.

###### Body ######
* Nenhum.

##### Resposta #####
* HTTP 200 em caso de sucesso.
* Uma lista com os SKUs disponíveis que possuem preço entre 10.0 e 40.0.

### Operações de Importação ###

As operações de importação recebem notificações no formato:
```
#!json

{
  "tipo": "criacao_sku",
  "dataEnvio": "2015-07-14T13:56:36",
  "parametros": {
    "idProduto": 100,
    "idSku": 200
  }
}
```

A partir desses Jsons de notificações, é feita uma consulta no [Catálogo de Produtos](https://sandboxmhubapi.epicom.com.br/v1/Api/GET-marketplace-produtos-idProduto-skus-id) disponível na API da Epicom, e então as informações sobre o SKU são carregas e persistidas localmente para futuras consultas.

### Operações de Importação ###

#### POST /sku/import ####
* Importa um objeto SKU a partir de uma notificação de criação de SKU.
##### Requisição #####

###### Parâmetros na URL ######
* Nenhum.

###### Body ######
* Formato:
```
#!json

{
  "tipo": "criacao_sku",
  "dataEnvio": {dataEnvio},
  "parametros": {
    "idProduto": {idProduto},
    "idSku": {idSku}
  }
}
```


##### Resposta #####
* HTTP 201 em caso de sucesso.
* Uma referência para o recurso SKU importado.

#### POST /sku/importList ####
* Importa uma lista de SKUs a partir de uma lista de notificações de criação de SKU.

##### Requisição #####

###### Parâmetros na URL ######
* Nenhum.

###### Body ######
* Formato:
```
#!json
[{
  "tipo": "criacao_sku",
  "dataEnvio": {dataEnvio1},
  "parametros": {
    "idProduto": {idProduto1},
    "idSku": {idSku1}
  }
},
...
{
  "tipo": "criacao_sku",
  "dataEnvio": {dataEnvioN},
  "parametros": {
    "idProduto": {idProdutoN},
    "idSku": {idSkuN}
  }
}]

```


##### Resposta #####
* HTTP 201 em caso de sucesso.
* Uma lista com os recursos SKUs importados.

## Melhorias ##
Algumas melhorias podem ser feitas para tornar o projeto mais robusto. Entretanto, visto que esse artefato é apenas um protótipo, decidi não implementa-las nessa primeira versão para deixar a lógica mais simples e condizente com o que foi proposto no desafio. Dentre algumas melhorias estão:

* Durante importação, cachear as notificações de importação de SKU por algum tempo, caso a conexão com a API da Epicom não seja estabelecida.
* Adicionar proteção contra CSRF.
* Melhorar a documentação da API.

## Com quem falar? ##
Desenvolvido por Raphael Zamith - rmzamith@gmail.com